/*
Navicat MySQL Data Transfer

Source Server         : show
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : dt-zero

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2020-02-28 18:21:31
*/

CREATE DATABASE IF NOT EXISTS dt-zero default charset utf8mb4 COLLATE utf8_general_ci;

use dt-zero;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for event_record
-- ----------------------------
DROP TABLE IF EXISTS `event_record`;
CREATE TABLE `event_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_no` varchar(64) DEFAULT NULL COMMENT '事件号，唯一',
  `event_content` varchar(500) DEFAULT NULL COMMENT 'json格式的事件内容',
  `event_status` int(11) DEFAULT NULL COMMENT '0-未处理；1-处理完成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `amount` decimal(20,2) DEFAULT NULL COMMENT '账号余额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
