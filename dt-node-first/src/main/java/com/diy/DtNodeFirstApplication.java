package com.diy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJms
@MapperScan(basePackages = "com.diy.mapper")
@EnableScheduling
public class DtNodeFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtNodeFirstApplication.class, args);
	}

}
