package com.diy.service;

import com.diy.entity.EventRecord;

/**
 * Created by show on 2020/2/27.
 */
public interface TransferService {
	void doResolve(EventRecord eventRecord);

	void transferAmount(EventRecord param);
}
