package com.diy.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.diy.entity.EventRecord;
import com.diy.entity.UserInfo;
import com.diy.mapper.EventRecordMapper;
import com.diy.mapper.UserInfoMapper;
import com.diy.service.TransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Objects;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 15:53
 */
@Service
@Slf4j
public class TransferServiceImpl implements TransferService{

	@Autowired
	private EventRecordMapper eventRecordMapper;

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Autowired
	private TransferService transferService;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void doResolve(EventRecord param) {
		/**
		 * 1、消费消息
		 * 	1) 根据uid查询事件表,如果存在,则结束
		 * 	2) 插入事件表(防重insert,支持幂等性)
		 * 	3) 消息确认  暂时不考虑
		 *
		 * 2、转账
		 * 	1) 给郭芙蓉账户增加100两银子
		 * 	2) 更新事件表的状态为DONE，乐观锁更新，防止定时任务重复执行
		 *
		 * 3、定时任务
		 * 	1) 调用转账接口
		 */

		EventRecord eventRecordEntity = eventRecordMapper.queryRecordByNo(param.getEventNo());

		if(eventRecordEntity != null){
			//说明已经消费过，不处理
			log.error("11111111--已经消费过，不处理");
		}else{
			EventRecord eventRecord = new EventRecord();
			eventRecord.setEventNo(param.getEventNo());
			eventRecord.setEventContent(JSON.toJSONString(param));
			//状态：待处理
			eventRecord.setEventStatus(0);

			int insertResult = eventRecordMapper.insertWithNoRepeat(eventRecord);

			if(!Objects.equals(insertResult,1)){
				log.warn("2222222--重复的eventNo：{}",eventRecord.getEventNo());
				// 手动回滚事物
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return;
			}

			// 2 转账
			param.setId(eventRecord.getId());
			try{
				transferService.transferAmount(param);
			}catch (Exception e){
				log.error("转账异常，不影响事件表插入:{}",param);
			}

		}

	}

	/**
	 * 新开启一个事务
	 * @param eventRecord
	 */
	@Override
	@Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRES_NEW)
	public void transferAmount(EventRecord eventRecord) {

		JSONObject jsonObject = JSON.parseObject(eventRecord.getEventContent());

		UserInfo parse = JSON.parseObject(jsonObject.getString("eventContent"),UserInfo.class);

		UserInfo userInfo = new UserInfo();
		userInfo.setUserName(parse.getUserName());
		userInfo.setAmount(parse.getAmount());
		Integer updateResult = userInfoMapper.updateAmountNext(userInfo);

		if(!Objects.equals(updateResult,1)){
			log.error("转账失败,{}",userInfo);
			// 手动回滚事物
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return;
		}


		Integer updateEventStatusById = eventRecordMapper.updateEventStatusById(eventRecord.getId());

		if(!Objects.equals(updateEventStatusById,1)){
			log.error("更新事件表状态失败,{}",eventRecord.getId());
			// 手动回滚事物
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return;
		}
	}

	//每隔一分钟执行一次
	@Scheduled(cron = "0 0/1 * * * *")
	public void transferNextNode(){

		List<EventRecord> list = eventRecordMapper.queryUnResolvedRecord();

		list.stream().forEach(obj->{

			transferService.transferAmount(obj);

		});

	}
}
