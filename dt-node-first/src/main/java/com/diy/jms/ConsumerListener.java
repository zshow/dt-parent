package com.diy.jms;

import com.alibaba.fastjson.JSON;
import com.diy.entity.EventRecord;
import com.diy.service.TransferService;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.Serializable;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 15:46
 */
@Component
@Slf4j
public class ConsumerListener implements MessageListener {

	@Autowired
	private TransferService transferService;

	@Override
	@JmsListener(destination = "show111")
	public void onMessage(Message message) {
		String msg = null;
		try {
			/*ActiveMQObjectMessage activeMQMessage = (ActiveMQObjectMessage)message;
			EventRecord eventRecord = (EventRecord) activeMQMessage.getObject();*/

			msg = ((TextMessage) message).getText();
			log.info("recieve message:{}",msg);
			EventRecord eventRecord = JSON.parseObject(msg,EventRecord.class);
			log.info("eventRecord:{}",eventRecord);
			transferService.doResolve(eventRecord);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
