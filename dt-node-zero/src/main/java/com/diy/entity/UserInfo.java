package com.diy.entity;

import java.math.BigDecimal;

public class UserInfo {
    /**
     * 
     */
    private Integer id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 账号余额
     */
    private BigDecimal amount;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户名
     * @return user_name 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 账号余额
     * @return amount 账号余额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 账号余额
     * @param amount 账号余额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}