package com.diy.entity;

import java.io.Serializable;

public class EventRecord implements Serializable{
    /**
     * 
     */
    private Integer id;

    /**
     * 事件号，唯一
     */
    private String eventNo;

    /**
     * json格式的事件内容
     */
    private String eventContent;

    /**
     * 0-未处理；1-处理完成
     */
    private Integer eventStatus;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 事件号，唯一
     * @return event_no 事件号，唯一
     */
    public String getEventNo() {
        return eventNo;
    }

    /**
     * 事件号，唯一
     * @param eventNo 事件号，唯一
     */
    public void setEventNo(String eventNo) {
        this.eventNo = eventNo == null ? null : eventNo.trim();
    }

    /**
     * json格式的事件内容
     * @return event_content json格式的事件内容
     */
    public String getEventContent() {
        return eventContent;
    }

    /**
     * json格式的事件内容
     * @param eventContent json格式的事件内容
     */
    public void setEventContent(String eventContent) {
        this.eventContent = eventContent == null ? null : eventContent.trim();
    }

    /**
     * 0-未处理；1-处理完成
     * @return event_status 0-未处理；1-处理完成
     */
    public Integer getEventStatus() {
        return eventStatus;
    }

    /**
     * 0-未处理；1-处理完成
     * @param eventStatus 0-未处理；1-处理完成
     */
    public void setEventStatus(Integer eventStatus) {
        this.eventStatus = eventStatus;
    }
}