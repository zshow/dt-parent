package com.diy.service.impl;

import com.alibaba.fastjson.JSON;
import com.diy.dto.TransferDTO;
import com.diy.entity.EventRecord;
import com.diy.mapper.EventRecordMapper;
import com.diy.mapper.UserInfoMapper;
import com.diy.service.TransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.messaging.Message;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 12:13
 */
@Service
@Slf4j
public class TransferServiceImpl implements TransferService {

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Autowired
	private EventRecordMapper eventRecordMapper;

	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void transfer(TransferDTO transferDTO) {
		/**
		 * 1、从账号扣钱
		 * 2、插入事件表
		 */

		Integer updateResult = userInfoMapper.updateAmount(transferDTO);

		if(!Objects.equals(updateResult,1)){
			// 手动回滚事物
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return;
		}
		log.info("update user_info success,transferDTO:",transferDTO);

		EventRecord eventRecord = new EventRecord();
		eventRecord.setEventNo(UUID.randomUUID().toString().replace("-",""));
		eventRecord.setEventContent(JSON.toJSONString(transferDTO));
		//状态：待处理
		eventRecord.setEventStatus(0);

		int insertResult = eventRecordMapper.insertSelective(eventRecord);

		if(!Objects.equals(insertResult,1)){
			// 手动回滚事物
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return;
		}
		log.info("insert event_record success,eventRecord:",eventRecord);
	}

	//每隔一分钟执行一次
	@Scheduled(cron = "0 0/1 * * * *")
	@Override
	public void transferNextNode(){

		List<EventRecord> list = eventRecordMapper.queryUnResolvedRecord();
		
		String destinationName1 = "show111";
		String destinationName2 = "show222";
		
		list.stream().forEach(obj->{

//			jmsTemplate.send(destinationName1,session -> session.createObjectMessage(obj));
//			jmsMessagingTemplate.convertAndSend(destinationName1,JSON.toJSON(obj));
			jmsTemplate.send(destinationName1,session -> session.createTextMessage(JSON.toJSONString(obj)));
			obj.setEventStatus(1);

			eventRecordMapper.updateEventStatusById(obj);

		});

	}
}
