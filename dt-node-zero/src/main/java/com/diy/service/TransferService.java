package com.diy.service;

import com.diy.dto.TransferDTO;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 12:12
 */
public interface TransferService {

	void transfer(TransferDTO transferDTO);

	void transferNextNode();
}
