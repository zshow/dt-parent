package com.diy.controller;

import com.diy.dto.TransferDTO;
import com.diy.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 12:02
 */
@RestController
@RequestMapping("/mq")
public class MQController {


	@Autowired
	private TransferService transferService;
	/**
	 * 转账
	 * @param transferDTO
	 * @return
	 */
	@RequestMapping(value = "/transfer", method = RequestMethod.GET)
	public Object applyClose(TransferDTO transferDTO) {


		transferService.transfer(transferDTO);

		return "transfer success";

	}

	@RequestMapping(value = "/maunalTransferNextNode", method = RequestMethod.GET)
	public Object maunalTransferNextNode() {


		transferService.transferNextNode();

		return "transfer success";

	}
}
