package com.diy.mapper;

import com.diy.entity.EventRecord;

import java.util.List;

public interface EventRecordMapper {
    /**
     *
     * @mbggenerated 2020-02-27
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int insert(EventRecord record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int insertSelective(EventRecord record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    EventRecord selectByPrimaryKey(Integer id);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int updateByPrimaryKeySelective(EventRecord record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int updateByPrimaryKey(EventRecord record);

	List<EventRecord> queryUnResolvedRecord();

    Integer updateEventStatusById(EventRecord obj);
}