package com.diy.mapper;

import com.diy.dto.TransferDTO;
import com.diy.entity.UserInfo;

public interface UserInfoMapper {
    /**
     *
     * @mbggenerated 2020-02-27
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int insert(UserInfo record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int insertSelective(UserInfo record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    UserInfo selectByPrimaryKey(Integer id);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int updateByPrimaryKeySelective(UserInfo record);

    /**
     *
     * @mbggenerated 2020-02-27
     */
    int updateByPrimaryKey(UserInfo record);

    Integer updateAmount(TransferDTO transferDTO);
}