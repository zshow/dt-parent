package com.diy.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author show
 * @Date 2020/2/27 12:08
 */
@Data
public class TransferDTO implements Serializable{

	/**
	 * to username
	 */
	private String userName;

	/**
	 * 金额
	 */
	private BigDecimal amount;

	/**
	 * 事件唯一标识
	 */
	private String evnetNo;
}
