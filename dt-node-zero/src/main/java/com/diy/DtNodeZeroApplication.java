package com.diy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan(basePackages = "com.diy.mapper")
@EnableScheduling
@EnableJms
public class DtNodeZeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtNodeZeroApplication.class, args);
	}

}
